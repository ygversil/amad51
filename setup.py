"""Python training level 1 setup module."""

from setuptools import setup, find_packages
from codecs import open
import os.path as osp

here = osp.abspath(osp.dirname(__file__))

with open(osp.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='python_lvl2',
    version='0.1.dev0',
    description='Advanced Object Oriented Python',
    long_description=long_description,
    url='https://github.com/pypa/sampleproject',
    author='Logilab',
    author_email='formation@logilab.fr',
    license='Copyright (C) Logilab SA - All Rights Reserved',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Other Audience',
        'Topic :: Education',
        'Topic :: Software Development',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='python developement advanced',
    packages=find_packages(exclude=[]),
    install_requires=[],
    extras_require={},
    setup_requires=['pytest-runner', 'flake8'],
    tests_require=['pytest'],
    package_data={
        'tests': ['data/access.log'],
    },
    data_files=[],
    entry_points={},
)
