"""LogEntry class."""

import datetime
import re

ATTRIBUTES = (
    'ip_client',
    'user_identifier',
    'username',
    'date',
    'verb',
    'path',
    'protocol',
    'resp_code',
    'resp_len',
    'referrer',
    'user_agent',
    'req_len',
)


def parse_alog_line(line):
    """Factory function returning a LogEntry's instance."""
    match = re.match(r'(?P<ip_client>\d+\.\d+\.\d+\.\d+)\s+'
                     r'(?P<user_identifier>.+)\s+'
                     r'(?P<username>.+)\s+'
                     r'\[(?P<date>.+)\]\s+'
                     r'"(?P<verb>[A-Z]+)\s+'
                     r'(?P<path>.+)\s+'
                     r'(?P<protocol>.+)"\s+'
                     r'(?P<resp_code>\d+)\s+'
                     r'(?P<resp_len>.+)\s+'
                     r'"(?P<referrer>.+)"\s+'
                     r'"(?P<user_agent>.+)"\s+'
                     r'(?P<req_len>\d+)', line)
    if not match:
        raise ValueError('Malformed line:\n{0}'.format(line))
    params = match.groupdict()
    for k, v in params.items():
        if v == '-':
            params[k] = None
    for key in ('resp_code', 'resp_len', 'req_len'):
        value = params[key]
        if value is not None:
            params[key] = int(value)
    # Convert the string timestamp into datetime
    timestamp = params.get('date')
    if timestamp:
        timestamp, tz = timestamp.split()
        timestamp = datetime.datetime.strptime(timestamp, '%d/%b/%Y:%H:%M:%S')
        match = re.match(r'([\-+])?(\d\d)(\d\d)', tz)
        tz = int(match.group(2)) * 3600 + int(match.group(3)) * 60
        if match.group(1) == '-':
            tz = -tz
        utc_timestamp = timestamp - datetime.timedelta(tz)
        params['date'] = utc_timestamp
    return LogEntry(**params)


class LogEntry(object):
    """A LogEntry instance represent an Apache access log line."""

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        if self.resp_len is not None:
            self._kbytes_resp_len = self.resp_len / 1024.

    def match(self, **kwargs):
        for k, v in kwargs.items():
            if self.__dict__.get(k) != v:
                return False
        return True

    @property
    def kbytes_resp_len(self):
        return self._kbytes_resp_len

    @kbytes_resp_len.setter
    def kbytes_resp_len(self, value):
        self.resp_len = value * 1024

    def to_dict(self):
        d = self.__dict__.copy()
        dt = d.get('date')
        if dt:
            d['date'] = dt.strftime('%d/%b/%Y:%H:%M:%S')
        d.pop('_kbytes_resp_len')
        return d

    def to_tuple(self):
        return tuple(getattr(self, key) for key in ATTRIBUTES)

    def __setattr__(self, name, value):
        if name == 'resp_len':
            super(LogEntry, self).__setattr__('_kbytes_resp_len',
                                              value / 1024.)
        super(LogEntry, self).__setattr__(name, value)

    def __str__(self):
        return ('{ip_client} {user_identifier} {username} [{date}] '
                '"{verb} {path} {protocol}" {resp_code} {resp_len} '
                '"{referrer}" "{user_agent}" {req_len}'
                ''.format(**dict((k, v if v is not None else '-')
                                 for k, v in self.__dict__.items())))

    def __eq__(self, other):
        return self.match(**other.__dict__) and other.match(**self.__dict__)

    def __ne__(self, other):
        return not self.__eq__(other)


def log_entries(fpath):
    with open(fpath) as f:
        for i, line in enumerate(f):
            try:
                logentry = parse_alog_line(line)
            except ValueError:
                print('Malformed line {0}'.format(i + 1))
            else:
                yield logentry
