
"""Manage connections to database."""

import sqlite3
from contextlib import closing

from python_lvl2.log_entry import log_entries


def db_init(dbpath):
    with sqlite3.connect(dbpath) as cnx:
        with closing(cnx.cursor()) as cur:
            cur.execute('DROP TABLE IF EXISTS log_entry')
            cur.execute('CREATE TABLE log_entry ('
                        'ip_client text, '
                        'user_identifier text, '
                        'username text, '
                        'date text, '
                        'verb text, '
                        'path text, '
                        'protocol text, '
                        'resp_code int, '
                        'resp_len int, '
                        'referrer text, '
                        'user_agent text, '
                        'req_len int'
                        ')')


def db_append(dbpath, fpath):
    with sqlite3.connect(dbpath) as cnx:
        with closing(cnx.cursor()) as cur:
            cur.executemany(
                'INSERT INTO log_entry(ip_client, user_identifier, username, '
                'date, verb, path, protocol, resp_code, resp_len, referrer, '
                'user_agent, req_len) '
                'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                (logentry.to_tuple() for logentry in log_entries(fpath))
            )
