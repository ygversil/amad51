import unittest

from python_lvl2.log_entry import LogEntry, parse_alog_line


class LogEntryTC(unittest.TestCase):

    def setUp(self):
        self.sample_line = (
            '127.0.0.1 - frank [10/Oct/2000:13:55:36 -0700] '
            '"GET /apache_pb.gif HTTP/1.0" 200 2326 "-" '
            '"check_http/v1.4.15 (nagios-plugins 1.4.15)" 530'
        )
        self.sample_params = {'ip_client': '127.0.0.1',
                              'user_identifier': None,
                              'username': 'frank',
                              'date': '10/Oct/2000:13:55:36 -0700',
                              'verb': 'GET',
                              'path': '/apache_pb.gif',
                              'protocol': 'HTTP/1.0',
                              'resp_code': '200',
                              'resp_len': 2326,
                              'referrer': None,
                              'user_agent': 'check_http/v1.4.15 '
                              '(nagios-plugins 1.4.15)',
                              'req_len': 530}

    def test_parse_alog_line(self):
        logentry = parse_alog_line(self.sample_line)
        self.assertEqual(logentry.ip_client, '127.0.0.1')
        self.assertEqual(logentry.referrer, None)

    def test_print(self):
        logentry = LogEntry(**self.sample_params)
        self.assertEqual(logentry.__str__(), self.sample_line)

    def test_match(self):
        logentry = LogEntry(**self.sample_params)
        self.assertTrue(logentry.match(ip_client='127.0.0.1'))
        self.assertTrue(logentry.match(referrer=None))
        self.assertFalse(logentry.match(ip_client='122.0.0.1'))
        self.assertFalse(logentry.match(foo='bar'))

    def test_eq(self):
        logentry1 = LogEntry(**self.sample_params)
        logentry2 = LogEntry(**self.sample_params)
        self.assertEqual(logentry1, logentry2)
        other_params = self.sample_params.copy()
        other_params['ip_client'] = '122.0.01'
        logentry3 = LogEntry(**other_params)
        self.assertNotEqual(logentry1, logentry3)
        other_params = self.sample_params.copy()
        other_params['foo'] = 'bar'
        logentry4 = LogEntry(**other_params)
        self.assertNotEqual(logentry1, logentry4)


if __name__ == '__main__':
    unittest.main()
